#!/bin/bash

USERNAME='botlabovics'

function getLocation {
    RESPONSE=$(curl 192.168.100.8:5000/userlocation/$USERNAME -s)
    LOCATION=$(echo $RESPONSE | jq '.address.address')
}

function setRTPEnpoint {
    echo "New location: $LOCATION"
    if [ ! -z "$MODULE_NUMB" ] 
    then
        pactl unload-module $MODULE_NUMB
    fi
    MODULE_NUMB=$(pactl load-module module-rtp-send source=@DEFAULT_SOURCE@ destination_ip=$LOCATION port=20001)
}

function clearRTPModules {
    echo "Clearing previous RTP-send modules"
    pactl unload-module module-rtp-send
}

clearRTPModules

while true
do 
    getLocation
    
    if [ "$LOCATION" != "$PREVIOUS_LOCATION" ]; then
        setRTPEnpoint
    fi

    PREVIOUS_LOCATION=$LOCATION

    sleep 1
done

