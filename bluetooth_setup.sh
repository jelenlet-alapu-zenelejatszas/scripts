#! /bin/bash

sudo apt update
sudo apt install bluez pulseaudio-module-bluetooth

sudo sh -c 'echo "Enable=Source,Sink,Media" >> /etc/bluetooth/main.conf'

sudo sh -c 'echo "Class = 0x00041C" >> /etc/bluetooth/main.conf'

sudo sh -c 'echo "resample-method = trivial" >> /etc/pulse/daemon.conf'

# pulseaudio -D

# pactl load-module module-bluetooth-discover

sudo service bluetooth restart